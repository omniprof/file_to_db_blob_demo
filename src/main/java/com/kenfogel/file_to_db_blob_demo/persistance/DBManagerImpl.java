package com.kenfogel.file_to_db_blob_demo.persistance;

import com.kenfogel.file_to_db_blob_demo.beans.PictureBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sample code for writing and reading a byte array to a db
 *
 * @author Ken Fogel
 */
public class DBManagerImpl implements DBManager {

    private final static Logger LOG = LoggerFactory.getLogger(DBManagerImpl.class);

    private final String url = "jdbc:mysql://localhost:3306/BINARYBLOBDEMO?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "TheUser";
    private final String password = "pancake";

    /**
     * Create a record in the DB from a PictureBean
     * @param picture
     * @return 
     * @throws java.sql.SQLException 
     */
    @Override
    public int create(PictureBean picture) throws SQLException {
        int result;
        String createQuery = "INSERT INTO BINARYBLOBFILES (FILENAME, BINARYDATA) VALUES (?,?)";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user,
                password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against SQL
                // Injection
                PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, picture.getFileName());
            ps.setBytes(2, picture.getPicture());

            result = ps.executeUpdate();
            // Retrieve generated primary key value and assign to bean
            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                picture.setId(recordNum);
                LOG.debug("New record ID is " + recordNum);
            }           
        }
        LOG.info("# of records created : " + result);
        return result;
    }

    /**
     * Find a record based on the ID field
     * @param id
     * @return 
     * @throws java.sql.SQLException
     */
    @Override
    public PictureBean findByID(int id) throws SQLException {
        // If there is no record with the desired id then this will be returned
        // as a null pointer
        PictureBean pictureBean = new PictureBean();

        String selectQuery = "SELECT FILENAME, BINARYDATA FROM BINARYBLOBFILES WHERE ID = ?";

        try (Connection connection = DriverManager.getConnection(url, user,
                password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection
                .prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setLong(1, id);
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {

                if (resultSet.next()) {
                    pictureBean = new PictureBean();

                    pictureBean.setId(id);
                    pictureBean.setFileName(resultSet.getString("FILENAME"));
                    pictureBean.setPicture(resultSet.getBytes("BINARYDATA"));
                }
            }
        }
        LOG.info("Found " + id);
        return pictureBean;
    }

}
