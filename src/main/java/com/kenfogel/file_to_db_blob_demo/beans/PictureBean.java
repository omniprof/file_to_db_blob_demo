package com.kenfogel.file_to_db_blob_demo.beans;

/**
 * This class models a db record that only contains a file name and a byte array
 *
 * @author Ken Fogel
 */
public class PictureBean {

    private int id;
    private String fileName;
    private byte[] picture;

    /**
     * Default Constructor
     */
    public PictureBean() {
        super();
        fileName = "";
        picture = new byte[0];
    }

    /**
     * Non-default constructor
     *
     * @param id
     * @param fileName
     * @param picture
     */
    public PictureBean(final int id, final String fileName, final byte[] picture) {
        super();
        this.id = id;
        this.fileName = fileName;
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(final byte[] picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "PictureBean [id=" + id + ", fileName=" + fileName + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PictureBean other = (PictureBean) obj;
        return id == other.id;
    }
}
