package com.kenfogel.file_to_db_blob_demo.tests;

import com.kenfogel.file_to_db_blob_demo.beans.PictureBean;
import com.kenfogel.file_to_db_blob_demo.persistance.DBManager;
import com.kenfogel.file_to_db_blob_demo.persistance.DBManagerImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DemoTestCase {

    // This is my local MySQL server, should be coming
    // from a properties file
    private final String url = "jdbc:mysql://localhost:3306/BINARYBLOBDEMO?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "TheUser";
    private final String password = "pancake";
    private DBManager manager;

    private final static Logger LOG = LoggerFactory.getLogger(DBManagerImpl.class);

    public DemoTestCase() {
        manager = new DBManagerImpl();
    }

    /**
     * In this test an image file is stored in a record
     *
     * @throws IOException
     * @throws SQLException
     */
    @Test
    public void readPictureIntoBeanFromDiskAndSaveToDB() throws IOException, SQLException {
        PictureBean pictureBean = new PictureBean();
        Path p = FileSystems.getDefault().getPath("", "2015-06-23-05-36-145-Rhine_Gorge-Rüdesheim-Cruise640x480.jpg");
        pictureBean.setPicture(Files.readAllBytes(p));

        pictureBean.setFileName("2015-06-23-05-36-145-Rhine_Gorge-Rüdesheim-Cruise640x480.jpg");

        manager.create(pictureBean);

        assertEquals("File and bytes not the same", 48630, pictureBean.getPicture().length);
    }

    /**
     * In this test
     *
     * @throws IOException
     * @throws SQLException
     */
    @Test
    public void readPictureBeanFromDatabase() throws IOException, SQLException {
        PictureBean pictureBean = new PictureBean();
        Path p = FileSystems.getDefault().getPath("", "2015-06-23-05-36-145-Rhine_Gorge-Rüdesheim-Cruise640x480.jpg");
        pictureBean.setPicture(Files.readAllBytes(p));

        pictureBean.setFileName("2015-06-23-05-36-145-Rhine_Gorge-Rüdesheim-Cruise640x480.jpg");

        manager.create(pictureBean);

        pictureBean = manager.findByID(1);

        assertEquals("File and bytes not the same", 48630, pictureBean.getPicture().length);

    }
  
    /**
     * This routine recreates the database for every test. This makes sure that
     * a destructive test will not interfere with any other test.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss who helped me out last winter with an issue with Arquillian. Look
     * up Arquillian to learn what it is.
     */
    @Before
    public void seedDatabase() {
        final String seedDataScript = loadAsString("CreateBinaryBlobTable.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password);) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<String>();
        try {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
