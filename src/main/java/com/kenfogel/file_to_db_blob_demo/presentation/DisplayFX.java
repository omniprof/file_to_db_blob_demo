package com.kenfogel.file_to_db_blob_demo.presentation;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.file_to_db_blob_demo.beans.PictureBean;
import com.kenfogel.file_to_db_blob_demo.persistance.DBManager;
import com.kenfogel.file_to_db_blob_demo.persistance.DBManagerImpl;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class DisplayFX extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(DisplayFX.class);

    /**
     * Create the Scene to contain the image and place it on the Stage
     *
     * @param primaryStage
     * @throws java.lang.Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        LOG.info("The program begins");
        TextArea textArea;
        Scene myScene;
        ImageView imageView = new ImageView();

        WritableImage writableImage = convertByteToImage(getThePictureBean().getPicture());
        if (writableImage != null) {
            imageView.setImage(writableImage);

            final HBox pictureRegion = new HBox();
            pictureRegion.getChildren().add(imageView);
            myScene = new Scene(pictureRegion);
        } else {
            textArea = new TextArea("Houston, we have a problem");
            textArea.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Consolas, monaco, monospace");
            myScene = new Scene(textArea);
        }

        primaryStage.setScene(myScene);
        primaryStage.setTitle("Image Viewer");
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    /**
     * Convert a byte array that represents an image to a WritableImage object
     * that can be displayed in a Scene
     *
     * @param byteImage
     * @return WritableImage displayable object
     * @throws IOException
     */
    private WritableImage convertByteToImage(byte[] byteImage) throws IOException {

        WritableImage writableImage = null;

        InputStream in = new ByteArrayInputStream(byteImage);
        BufferedImage bufferedImage = ImageIO.read(in);

        if (bufferedImage != null) {
            writableImage = new WritableImage(bufferedImage.getWidth(), bufferedImage.getHeight());
            PixelWriter pw = writableImage.getPixelWriter();
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                for (int y = 0; y < bufferedImage.getHeight(); y++) {
                    pw.setArgb(x, y, bufferedImage.getRGB(x, y));
                }
            }
        }
        return writableImage;
    }

    /**
     * Retrieve the record with the ID filed value of 1
     *
     * @return DB record with an image as a byte array inside
     */
    private PictureBean getThePictureBean() throws SQLException {
        DBManager dm = new DBManagerImpl();
        PictureBean pictureBean = dm.findByID(1);
        return (pictureBean);
    }

    /**
     * Lets get the party started
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
