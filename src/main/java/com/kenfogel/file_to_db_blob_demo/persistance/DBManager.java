package com.kenfogel.file_to_db_blob_demo.persistance;

import com.kenfogel.file_to_db_blob_demo.beans.PictureBean;
import java.sql.SQLException;

/**
 * Interface for db
 *
 * @author Ken Fogel
 */
public interface DBManager {

    // Insert a new record in DB
    public int create(PictureBean picture) throws SQLException;

    // Find a record based on the ID field
    public PictureBean findByID(int id) throws SQLException;

}
